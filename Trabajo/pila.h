#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pilas{
    private:
      int tam;
      int mid;
      int dbmid;

    public:
      /* constructores */
      Pilas();
      Pilas(int tam, int mid, int dbmid);

      /* set y get */
      int get_tam();
      int get_mid();
      int get_dbmid();
      void set_tam(int tam);
      void set_mid(int mid);
      void set_dbmid(int dbmid);

      /* funciones */
      void makearray(int *pila);
      void ShowArray (int *pila);
      void ReMove(int *pila);
};
#endif
