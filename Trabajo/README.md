# G2-U1
Para iniciar el programa:
1) Ingrese en el CMD el comando make para instalar el programa
-> .../g2-u1/Trabajo$ make

2) Se creará un programa con nombre de Cargo, para ingresar a este
   haga lo siguiente:
-> .../g2-u1/Trabajo$ ./Cargo


En el programa:
1) El programa consiste en casi simular un cargo, donde solo puedes sacar los
   numeros que se encuentran en la parte superior, en el caso que sean un par
   el numero será eliminado, pero si es impar, lo puede mover a otras pilas
   disponibles.

2) Primero se le pedirá al usuario el tamaño de la 'zona de cargo' que desea,
   el tamaño siempre sera un NxN, o en otras palabras un cuadrado.

3) Al ingresar el tamaño, el usuario será ingresado a un menu donde tendrá 3
   opciones, eliminar o mover un numero, ver el array que se tiene, en forma de
   pilas cada uno y cerrar el programa

4) La interacción del programa se encuentra en el eliminar o mover un numero,
   donde le usuario puede decidir que pila desea mover comenzando desde el 0,
   en el caso que el numero conseguido sea un par, será eliminado y será
   enviado al menu, si es impar, lo puede mover donde desee.

5) En el caso que ya haya realizado lo que deseaba con el programa, puede
   simplemente terminar el programa con la opcion de cerrar el programa.
   El programa no terminará por si solo, solo el usuario puede decidir cuando
   termina

Para borrar el programa:
1) Asegurese que esté en la misma carpeta donde corrio el programa
-> .../g2-u1/Trabajo

2) Ingrese el comando make clean para borrar el programa que uso
-> .../g2-u1/Trabajo$ make clean
