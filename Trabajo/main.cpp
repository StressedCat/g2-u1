#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
using namespace std;
#include "pila.h"


/* Info main
 * El usuario se le ofrecerá la opcion de ingresar un tamaño designado por este.
 * El programa tendrá un menu con 3 opciones, siendo ver el array, modificar el
 * array y terminar el programa.
 */
int main() {
  bool ShutDown = false;
  int opt;
  int tam;
  int medio;
  int dbmid;
  cout << "Bienvenido al programa, se busca eliminar los numeros pares" << endl;
  cout << "Ingrese el tamaño maximo de su matriz de pilas: ";
  cin >> tam;
  medio = tam/2;
  /* En el caso que sea 4 o un 2,
   * se colocará una opcion 'especial', ya que no funciona con las otras
   */
  if(tam == 4 or tam == 2){
    dbmid = medio/2;
  }
  if(tam%2 == 0 and tam/10 > 1){
    dbmid = medio-1;
  }
  /* cualquier otro numero, tendrá una formula general */
  else{
    dbmid = medio;
  }
  int pila[tam*tam];
  Pilas Cargo = Pilas(tam, medio, dbmid);
  Cargo.makearray(pila);

  /* Menu
   * Aca se encuentra el menu, donde el usuario puede ingresar las opciones que
   * desee
   */
  while (ShutDown == false) {
    cout << "[1] para retirar o mover" << endl;
    cout << "[2] para inspeccionar" << endl;
    cout << "[0] Salir del programa" << endl << "Ingrese su opcion: ";
    cin >> opt;
    cout << endl << "****************************************" << endl << endl;
    if (opt == 1) {
      Cargo.ReMove(pila);
    }
    else if(opt == 2){
      Cargo.ShowArray(pila);
    }
    else if(opt == 0) {
      ShutDown = true;
      cout << "El programa fue apagado" << endl;
    }
    else{
      cout << "opcion incorrecta" << endl;
    }
    cout << endl << "****************************************" << endl << endl;
  }
}
