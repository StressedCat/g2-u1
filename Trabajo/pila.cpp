#include <iostream>
using namespace std;
#include "pila.h"

/* constructores */
Pilas::Pilas(){
  int tam;
  int mid;
  int dbmid;
}

Pilas::Pilas(int tam, int mid, int dbmid){
  this->tam = tam;
  this->mid = mid;
  this->dbmid = dbmid;
}

/* get and set */
int Pilas::get_tam(){
  return this->tam;
}

int Pilas::get_mid(){
  return this->mid;
}

int Pilas::get_dbmid(){
  return this-> dbmid;
}

void Pilas::set_tam(int tam){
  this->tam = tam;
}

void Pilas::set_mid(int mid){
  this->mid = mid;
}

void Pilas::set_dbmid(int dbmid){
  this->dbmid = dbmid;
}


/* Info makearray
 * Para crear un array, se usarán numeros generados al azar, la matriz siempre
 * se llenará hasta la mitad o uno menos de la mitad en el caso que sea un
 * numero impar el tamaño, se rellenará con 0 los espacios 'vacios', donde los
 * numeros generados no pueden ser 0, y el 0 representaría un espacio vacio,
 * solo se generan entre el 1 al 9, para que al mostrar el array, no se vea
 * tan desordenado
 */
void Pilas::makearray(int *pila){
  int mid = this->mid;
  int tam = this->tam;
  int dbmid = this->dbmid;
  /* Seed de los numeros conseguidos, depende del tiempo */
  srand((unsigned) time(0));
  int num;
  for(int x = 0; x < tam*tam; x++){
    pila[x] = 0;
  }
  for(int x = 0; x < tam*tam; x++){
    /* Info del If
     * En el caso que se llega a la mitad, se hará un salto con dbmid
     * el cual varia segun el tamaño que se ingresó
     */
    if (x%tam == mid){
      x = (x + dbmid);
    }
    else if (x%tam != mid) {
      /* */
      num = 1 + (rand() % 9);
      pila[x] = num;
    }
  }
}


/* Info ShowArray
 * Se mostrará el array como se ve en forma de pilas, el array viene siendo
 * de una dimension, entonces para imitar uno de dos, se realizarán formulas
 * generales para la operacion de cada tamaño, la formula general consiste
 * en imprimir del tope de cada 'limite' que vendria siendo tam-1, dando
 * saltos para dar la ilusion que se tiene una array 2D
 */
void Pilas::ShowArray (int *pila){
  int tam = this->tam;
  int z = tam-1;
  while (z != -1){
    for (int x = 0; x < tam; x++) {
      cout << "[" << pila[(x*tam)+z] << "] ";
    }
    cout << endl;
    z = z - 1;
  }
}


/* Info ReMove
 * Se realiza la operacion de mover o en el caso especial, eliminar el numero
 * seleccionado, en el caso que el usuario ingrese una fila que este vacia o
 * incluso fuera de rango (1), se le avisará e incluso se hara un ciclo hasta
 * que el usuario ingrese lo correcto, tambien sucederá si el caso sea mover el
 * numero (2), donde le usuario deberá ingresar una fila, en este caso, la fila
 * debe tener espacio o que este dentro del rango, caso contrario, se le avisará
 * y estará en un ciclo hasta que la operacion sea correcta, ademas el programa
 * consiste en eliminar los numeros pares (3), en el caso que el numero obtenido
 * fue un par, será eliminado, el otro caso es simplemente que lo tenga que
 * mover
 */
void Pilas::ReMove(int *pila) {
  int tam = this->tam;
  bool flick;
  int z = -1;
  int num;
  flick = false;
  cout << "Ingrese que fila desea modificar, comenzando de la fila 0" << endl;
  /* Ciclo para ver si la fila tiene algun numero y dentro del rango (1) */
  while (flick != true) {
    cin >> z;
    if (pila[z*tam] != 0 && z < tam && z >= 0) {
      flick = true;
    }
    else{
      cout << "La fila no contiene ningun numero o esta fuera de rango" << endl;
    }
  }
  /*
   * Información de lo que se consiguio de la fila (3)
   * Ademas de esto se ve la fila de arriba hacia abajo, ignorando todo los 0
   * y llegando a un numero, en el caso que se toque un numero, el 'for' será
   * forzado a terminar
   */
  for (int x = tam-1; x > -1; x--) {
    if (pila[(z*tam)+x] != 0) {
      num = pila[(z*tam)+x];
      cout << "El numero conseguido fue: " << num << endl;
      pila[(z*tam)+x] = 0;
      x = -2;
    }
  }
  if (num%2 == 0) {
    cout << "Como el numero es par, será eliminado" << endl;
  }
  else{
    flick = false;
    cout << "A que fila desea mover el numero" << endl;
    /* Ciclo para ver si la fila tiene espacio y dentro del rango (2) */
    while (flick != true) {
      cin >> z;
      if (pila[((z+1)*tam)-1] == 0 && z < tam && z >= 0) {
        flick = true;
      }
      else{
        cout << "La fila esta llena o esta fuera de rango" << endl;
      }
    }
    /*
     * Se ve la fila de abajo hacia arriba, si llega a tocar un 0, se ingresa
     * el numero obtenido y se termina el for.
     */
    for (int x = 0; x < tam; x++) {
      if (pila[(z*tam)+x] == 0) {
        pila[(z*tam)+x] = num;
        x = tam;
      }
    }
  }
}
